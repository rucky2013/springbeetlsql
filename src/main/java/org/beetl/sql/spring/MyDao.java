package org.beetl.sql.spring;

public interface MyDao {
	public void save(User user);
	public int total(User user) ;
}
