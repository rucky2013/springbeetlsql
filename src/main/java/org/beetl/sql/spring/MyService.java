package org.beetl.sql.spring;

public interface MyService {
	public void saveUser(User user);
	public int total(User user);
}
