package org.beetl.sql.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by mikey.zhaopeng on 2015/6/2.
 */
@Controller
public class IndexController {

	@Autowired
	MyService service ;
    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public ModelAndView index() {
    	ModelAndView view = new ModelAndView("/index.html");
    	int total = service.total(new User());
    	view.addObject("total",total);
        return view;
    }
}
